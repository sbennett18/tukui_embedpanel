## Interface: 80000
## Title: Tukui Embed Panel for |cffff8000Tukui|r
## Notes: Upper left corner panel for use with AddOnSkins as embed window
## Version: 0.01
## Author: Kasharg
## RequiredDeps: Tukui, AddOnSkins
## Thanks to: Tukz, Hydra, Darth Predator, Azilroka, Flyingboots and all other members of Tukui.org community.
## X-Tukui-ProjectID: 36
## X-Tukui-ProjectFolders: Tukui_EmbedPanel

Tukui_EmbedPanel.lua
