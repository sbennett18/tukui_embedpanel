---------------------------------------------------------------
-- Tukui Embed Panel version 0.02 - Battle for Azeroth - 06-11-2018
---------------------------------------------------------------
local AS, ASL = unpack(AddOnSkins)
local T, C, L = Tukui:unpack()

local EmbedPanel
local TChatBGLeft, TChatBGBottom, TChatBGWidth, TChatBGHeight

-- local ScreenWidth, ScreenHeight = GetPhysicalScreenSize()
local orig_EmbedSystem_WindowResize = AS.EmbedSystem_WindowResize
local ScreenWidth, ScreenHeight = T.ScreenWidth, T.ScreenHeight

--------------------------------------------------------------------------------------
-- Tukui Embed Panel OPTIONS
--------------------------------------------------------------------------------------
C["Embed"] = {
	["Panel"] = true,
	-- ["EmbedPanelCombatHide"] = false,
}

local Embed = {
	["Panel"] = {
		["Name"] = "Embed Panel",
		["Desc"] = "Enable/Disable the Embed Panel for Tukui",
	},
	-- ["EmbedPanelCombatHide"] = {
	-- 	["Name"] = "In Combat Hide",
	-- 	["Desc"] = "Enable/Disable In Combat Hide function",
	-- },
}
TukuiConfig.enUS["Embed"] = Embed

--------------------------------------------------------------------------------------
-- Embed panel
--------------------------------------------------------------------------------------
hooksecurefunc(T.Panels, "Enable", function()
    TChatBGLeft, TChatBGBottom, TChatBGWidth, TChatBGHeight = T.Panels.LeftChatBG:GetRect()
	-- for i,v in ipairs({ScreenWidth, ScreenHeight, TChatBGLeft, TChatBGBottom, TChatBGWidth, TChatBGHeight}) do
	-- 	DEFAULT_CHAT_FRAME:AddMessage(v)
	-- end

	EmbedPanel = CreateFrame("Frame", "EmbedPanel", UIParent)
	EmbedPanel:Size(TChatBGWidth, TChatBGHeight)
	EmbedPanel:SetPoint("TOPLEFT", TChatBGLeft, -TChatBGBottom)
	EmbedPanel:SetFrameLevel(1)
	EmbedPanel:SetFrameStrata("BACKGROUND")
	EmbedPanel:CreateBackdrop("Transparent")
	EmbedPanel.Backdrop:CreateShadow()

	-- local Movers = T["Movers"]
	-- Movers:RegisterFrame(EmbedPanel)
end)

-- Replace AS:EmbedSystem_WindowResize function with new function
-- that use the new EmbedPanel
local function EmbedSystem_WindowResize()
    if not C.Embed.Panel then
        orig_EmbedSystem_WindowResize()
    else
        _G.EmbedSystem_MainWindow:ClearAllPoints()
        _G.EmbedSystem_MainWindow:SetPoint('TOPLEFT', EmbedPanel, 'TOPLEFT', 6, -6)
        _G.EmbedSystem_MainWindow:SetSize(TChatBGWidth - 12, TChatBGHeight - 12)

        _G.EmbedSystem_LeftWindow:SetPoint('RIGHT', _G.EmbedSystem_RightWindow, 'LEFT', -2, 0)
        _G.EmbedSystem_RightWindow:SetPoint('RIGHT', _G.EmbedSystem_MainWindow, 'RIGHT', 0, 0)
        _G.EmbedSystem_LeftWindow:SetSize(AS:CheckOption('EmbedLeftWidth') - 1, _G.EmbedSystem_MainWindow:GetHeight())
        _G.EmbedSystem_RightWindow:SetSize((_G.EmbedSystem_MainWindow:GetWidth() - AS:CheckOption('EmbedLeftWidth')) - 1, _G.EmbedSystem_MainWindow:GetHeight())

        if _G.Enhanced_Config and _G.Enhanced_Config.Options.args.addonskins then
            _G.Enhanced_Config.Options.args.addonskins.args.embed.args.EmbedLeftWidth.min = floor(_G.EmbedSystem_MainWindow:GetWidth() * 0.25)
            _G.Enhanced_Config.Options.args.addonskins.args.embed.args.EmbedLeftWidth.max = floor(_G.EmbedSystem_MainWindow:GetWidth() * 0.75)
        end
    end
end

hooksecurefunc(AS, "EmbedInit", function()
    AS.EmbedSystem_WindowResize = EmbedSystem_WindowResize

	if _G.EmbedSystem_MainWindow:IsShown() then
		EmbedPanel:Show()
	else
		EmbedPanel:Hide()
	end

	hooksecurefunc(_G.EmbedSystem_MainWindow, "Hide", function()
		if C.Embed.Panel then
			EmbedPanel:Hide()
		end
	end)

	hooksecurefunc(_G.EmbedSystem_MainWindow, "Show", function()
		if C.Embed.Panel then
			EmbedPanel:Show()
		end
	end)
end)

--------------------------------------------------------------------------------------
-- Convenience functional utilities
--------------------------------------------------------------------------------------
-- map(function, table)
-- e.g: map(double, {1,2,3})    -> {2,4,6}
local function map(func, tbl)
	local newtbl = {}
	for i,v in pairs(tbl) do
		newtbl[i] = func(v)
	end
	return newtbl
end
